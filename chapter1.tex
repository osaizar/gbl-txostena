\chapter{Introduction}\label{ch:chapter1}
\section{Background}
In the last years the introduction of Industry 4.0 and IoT devices has created a big demand of infrastructures that can process and store vast amounts of data. This is currently achieved by Big Data technologies that allow to store and compute the information distributed and in parallel. These technologies are a flexible, scalable and cheap way to process and store quantities of information that would have been impossible or very expensive to process in the past.

Nowadays, one of the most used Big Data platforms is Hadoop, a framework that uses Map Reduce to process the data and Hadoop Distributed File System (HDFS) to store data in different machines. 

One of the biggest concerns of technologies like Hadoop is that they don't offer a sufficiently secure access control system: a Hadoop cluster can have many users with different credentials that need access to different processes and files. Because of this, a secure way to control users and their permissions is needed.

Hadoop solves this with user permissions and Access Control Lists (ACL) together with some encryption (Transparent Encryption and Encryption Zones), however, this may not be enough for a system that requires complex access trees.

Fine-grained Access Control systems can be a way to provide enough flexibility to allow very complex access trees. These systems, facilitate granting differential access rights to a set of users and allow flexibility in specifying the access rights of individual users \cite{goyal2006attribute}. 

In order to achieve fine-grained access control in Hadoop, the objective of this project is to use an encryption algorithm suited for this purpose: Ciphertext-Policy Attribute-Based Encryption (CP-ABE). CP-ABE is an algorithm proposed by Bethencourt et al. \cite{bethencourt2007ciphertext} designed to secure data in distributed applications by providing control over encrypted data at a fine-grained scale.

In this project the integration of CP-ABE in the Apache Hadoop ecosystem is studied in order to achieve a fine-grained access control for a more secure distributed file storage system. A modified Hadoop version that uses CP-ABE and some benchmarks are provided.

\section{Objectives}\label{s:objectives}
The main objective of this project is to develop a modified Hadoop implementation that can provide fine-grained access control using Attribute Based-Encryption (ABE). Taking this in account, we defined the next secondary objectives:

\begin{itemize}
	\item Find the best ABE algorithm for our purpose.
	\item Develop a Hadoop version that:
		\begin{itemize}
			\item Controls the access to files using attributes.
			\item Encrypts the files at rest and at transit with ABE.
			\item Can be managed using Apache Ranger \cite{apacheranger}.
		\end{itemize}
	\item Test the system and provide performance measurements.
\end{itemize}

\section{Planning}
In order to achieve these objectives, it was a must to divide the development of this project in different phases. We distinguished five different phases for our project, Analysis, Design, Development, Testing and Validation and Documentation. In the same way, we divided each phase in different tasks that are necessary to finish the phase. All the tasks can be found in the figure \ref{fig:Tasks}.

\begin{figure}[H]
	\includegraphics[scale=0.5]{planning/tasks.png}
	\caption{Planned Phases and Tasks}
	\label{fig:Tasks}
\end{figure}

The different phases and tasks of the project have been used to create a Gantt diagram of the project that shows the order of the tasks and their estimated duration. The diagram can be found in the figure \ref{fig:GanttPrev}. As shown in the diagram, we thought that the most time consuming tasks would be the Analysis of the source code of Hadoop and the Documentation. The project needed to end in July 2018 so we decided to have some time to spare in order to be able to finish the project if some task took more time than expected.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{planning/gantt-prev.png}
	\caption{Planned Gantt Diagram}
	\label{fig:GanttPrev}
\end{figure}

After finishing the project, we can say that some tasks have taken much more time than expected and some others much less. As shown in the final Gantt diagram of the figure \ref{fig:GanttPost}, the main differences are that the development took less time than expected and that we needed a little more time than expected to finish most of the other tasks. We also added another task in the Testing and Validation phase aimed to analyze the results of the benchmarks. As a result, we had less time to work in the Documentation phase.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{planning/gantt-post.png}
	\caption{Final Gantt Diagram}
	\label{fig:GanttPost}
\end{figure}

\section{Economic Memory}
In this section we will show the different expenses of this project, divided in the different phases of the project. As show in the table \ref{tab:economicmem}, the only expenses of this project were the salary of the workers in it and the cost of the servers used in the testing and validation phase. The total cost of the project is of \textbf{7861 \euro}.

\begin{center}
    \begin{longtable}{ | l | p{7cm} | p{2cm} |}
    \hline
    \bf Phase & \bf Expense Name & Cost (\euro) \\ \hline
    Analysis & Salary & 1566 \\ \hline
    Design & Salary &  621 \\ \hline
    Development & Salary &  540 \\ \hline
    \multirow{3}{*}{Testing and Validation}
    	& Jupyter Server & 1225 \\ \cline{2-3}
    	& Testing Servers & 2100 \\ \cline{2-3}
    	& Salary & 1323 \\ \cline{2-3} \hline
    Documentation & Salary & 486 \\ \hline
    \multicolumn{2}{| l |}{\bf Total:} & \bf 7861 \\
    \hline
    \caption {Economic memory}
    \label{tab:economicmem}
    \end{longtable}
\end{center}

\section{Basic concepts} %% Etorkizuneko Oier, hau aldatu mesedez
This section includes some basic definitions of various concepts of CP-ABE and the Hadoop Ecosystem needed to fully understand the article.

\subsection{CP-ABE}\label{ss:cpabeconcept}
Ciphertext-Policy Attribute-Based Encryption (CP-ABE) is an encryption algorithm designed for complex access control on encrypted data. The access control is enforced using sets of attributes to identify users and policies to allow or deny the access to encrypted files. Because of that, this algorithm is a better access control solution for distributed systems than the traditional Public-key algorithms \cite{zhou2014new}.

\subsubsection{Components}
\paragraph{Attribute}
Attributes are descriptive credentials that are used to describe users. Each private key is associated with a set of attributes when it's created. Some examples of attributes can be: \textit{"dep-finances", "name-bob"} and \textit{"access-level = 2"}.

\paragraph{Policy}
A set of rules that dictate which keys can decrypt a given file. The policy is used to encrypt a file and it gets associated with it, only a key containing attributes that fulfill the policy can decrypt the file.

\paragraph{Master Key and Public Parameters}
The master key (MK) and public parameters (PK) are the two base keys that are needed to create private keys or to encrypt files. For example, the PK is used alongside a policy to encrypt a file, then, to decrypt the file a private key needs to be created with the MK and the PK of the same set, combined with attributes that fulfill the policy.

\subsubsection{Policy and Attribute types}\label{sss:cpabepolicytypes}
CP-ABE supports different types of policies and attributes to allow a more flexible access control. CP-ABE takes two kinds of policies and attributes: simple and numeric.

\paragraph{Simple}
Simple policies are just words like \textit{"foobar"}, \textit{"bob"} or \textit{"department-it"}, this implies that a user needs to have the same simple attribute in order to fulfill it.

\paragraph{Numeric}
Numeric attributes are attributes that have a numeric value like \textit{"access-level = 2"} and \textit{"age = 30"}, numeric policies in the other side, are policies that restrict a set of numeric attributes. They are formed by an attribute followed by a smaller-than (<) sign or a bigger-than (>) sign and a number. For example, some numeric policies could be: \textit{"age < 60"}, \textit{"date > 1530005413"} and \textit{"access-level > 2"}. In order to fulfill these policies, the user needs to have a numeric attribute that coincides both in name and in value with the policy.

\paragraph{Combining Policies}
These types of policy can be joined with the operands \textit{and} and \textit{or} to create more complex policies like \textit{"age < 60 and bob"}, \textit{"foobar or department-it"} or \textit{"access-level > 2 and date > 1530005413 and department-it"}. Lastly, the policies can also be nested using parenthesis and brackets: \textit{"access-level > 2 or (foobar and (department-it or bob))"}.

\subsection{Hadoop Ecosystem}
The Hadoop Ecosystem consists of different components like HDFS, Key Management Server (KMS) and Hadoop itself. In this section we will explain some parts that are needed to understand this document. All the information has been taken from the documentation websites \cite{apachehadoop}, \cite{hdfsarchitecture}, \cite{hdfstransparentenc} and  \cite{hadoopkms}.

\paragraph{Hadoop}
The Apache Hadoop software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models. It is designed to scale up from single servers to thousands of machines, each offering local computation and storage.

\paragraph{HDFS}\label{p:HDFS}
HDFS is a highly fault-tolerant distributed file system that is included in the Apache Hadoop project. It has a master/slave architecture and it's designed to be ran in a cluster formed by a NameNode and various DataNodes.

\paragraph{NameNode}\label{p:NameNode}
Master server in a HDFS cluster. It regulates file access and the file system namespace. It's responsible for executing operations like opening, closing, and renaming files and directories and it only stores the metadata of the files stored in HDFS.

\paragraph{DataNode}
Slave server in a HDFS cluster that is responsible of storing all the files in the system. When a file is uploaded to HDFS, it's split  into one or more blocks. These blocks are stored in a set of DataNodes.

\paragraph{Transparent Encryption}\label{par:transparentencryption}
The encryption in HDFS is implemented using transparent encryption, this means that the files are encrypted seamlessly for the users and the processes using them. The files are encrypted and decrypted \textit{end-to-end} by the clients so HDFS never has access to the decrypted files or the decrypted keys, this way, data is kept secure both in transit and at rest.

\paragraph{Encryption Zones}
The transparent encryption introduces a new concept to HDFS, the Encryption Zone (EZ). An encryption zone is a folder whose files are encrypted when they are copied to the folder and decrypted when a client needs to read them. Each encryption zone is associated to a encryption key, this key is used in the encryption and decryption of all the files in the zone.

\paragraph{EDEK and DEK}\label{p:edekanddek}
To understand how the encryption works in HDFS, is important to know what the Data Encryption Key (DEK) and the Encrypted Data Encryption Key (EDEK) are. The DEK is the encryption key that is used to encrypt a file in a encryption zone, this key is unique for each file and it's generated when the file is copied to the zone. Once the file is encrypted and stored in HDFS, the DEK is encrypted using the key of the encryption zone and the EDEK is created. The EDEK is stored in the file's metadata, so to access the file is necessary to decrypt the EDEK with the key of the zone, and decrypt the file using the DEK.

\paragraph{The KMS}\label{p:kms}
As the encryption keys can be shared between different encryption zones, the clients can't have access to these keys. To solve this problem all the keys are created and stored in a different server, the Key Management Server (KMS). When a new key is needed, the client sends a request to the KMS with the name of the key, the algorithm that needs to be used and the length of the needed key, then the KMS creates a key and stores it with the given name. That name will be used in the future to reference the key. For example, when the client needs to decrypt a EDEK, sends a JSON request with the EDEK and the key name to the KMS. The KMS can now find the key associated with that name and decrypt the EDEK, when done, the DEK is returned to the client. A representation of this architecture can be found in the figure \ref{fig:HDFSEZ}.

The KMS also includes a two-level ACL system: KMS ACLs and Key ACLs. In the first level, the KMS ACLs, the users are granted or denied permissions to manage the keys in the KMS, with these ACLs, an user could have permission to create a key but no to delete or read a key for example. In the same way, key ACLs can be configured to control the access to each key separately.

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{HDFSandZones.png}
	\caption{HDFS, Encryption Zones and KMS}
	\label{fig:HDFSEZ}
\end{figure}


\section{State of the art}
In this section we will analyze the state of the art of Access Control mechanisms in Hadoop. We divided this section in two parts: 1) the first part describes the encryption algorithms that lead us to CP-ABE and CP-ABE itself, 2) while the second part comments on the the different approaches to Access Control mechanisms in Hadoop.

\subsection{CP-ABE} \label{ss:stateofartCpabe}
\paragraph{Fuzzy Identity-Based Encryption}
(Fuzzy-IBE) is an error-tolerant encryption mechanism first introduced by Amit Sahai and Brent Waters \cite{sahai2005fuzzy}. Fuzzy-IBE allows to use different encryption and decryption keys if both keys are close to each other. A common use case is the biometric identity based encryption, where there are inherent differences each time a biometric identity is sampled.

Apart from this, Fuzzy-IBE can be used in a type of application that Sahai et al. call "Attribute-based Encryption". This new application creates user identities   based in the attributes that they have and allows a file to be encrypted using a certain identity, this way, only the users with the same set of attributes will be able to decrypt it.

Although this document is one of the first mentions to ABE, Sahai and Waters already discuss the utility of this type of algorithms to store and share data in untrusted servers.

\paragraph{Attribute-Based Encryption for Fine-Grained Access Control of Encrypted Data}
Vipul Goyal et al. present a new encryption algorithm called "Key Based Attribute-Based Encryption" (KP-ABE)  \cite{goyal2006attribute}, as a follow up to the previous paper by Sahai et al. \cite{sahai2005fuzzy}, Goyal et al. say that while the cryptosystem by Sahai et al. was shown to be useful for error-tolerant encryption with biometrics, the lack of expressibility seems to limit its applicability to large systems.

To solve this, KP-ABE allows for a fine-grained access control of the encrypted data as the attributes can be joined with operands like \textit{and} or \textit{or} to create access structures. This access structures are associated with user keys and determine which files can be decrypted, depending on the attributes the files are encrypted with.

For example, if Alice has a key associated with the access structure "finances-department or it-department" she will be able to decrypt a file encrypted with the attribute "it-department" but she won't be able to decrypt a file encrypted with the attribute "sales-department".

With this new cryptosystem, Goyal et al. intend to solve some limitations of the classic Public Key encryption: "If a third party must access the data for a set, a user of that set either needs to act as an intermediary and decrypt all relevant entries for the party or must give the party its private decryption key, and thus let it have access to all entries" \cite{goyal2006attribute}. Using the access trees of KP-ABE, these problems are solved easily.

\paragraph{Ciphertext-Policy Attribute-Based Encryption}
To continue with ABE, John Bethencourt, Amit Sahai and Brent Waters presented "Ciphertext-Policy Attribute-Based Encryption" (CP-ABE) \cite{bethencourt2007ciphertext} as an alternative to KP-ABE.

This algorithm is based in the same principles as KP-ABE with the same objectives in mind. The main difference is that while in KP-ABE the access structures were associated with the user keys and the files were encrypted using attributes, CP-ABE acts the other way around, the files are encrypted using access structures and the keys are associated to certain attributes. In words of Bethencourt et al. this happens because "We stress that in key-policy ABE, the encryptor exerts no control over who has access to the data she encrypts, except by her choice of descriptive attributes for the data".

\label{par:cpabetoolkit}
With the introduction of CP-ABE, Bethencourt et al. also present an implementation of their algorithm. The \textit{CP-ABE toolkit} can be downloaded from \cite{cpabetoolkit} and once compiled it offers an easy way to try CP-ABE or to apply it in larger systems.

\subsection{Access Control in Hadoop}

\paragraph{Securing Hadoop in Cloud} % yu 2014
Xianqing Yu et al. \cite{Yu:2014:SHC:2600176.2600202} found two vulnerabilities in Hadoop: overload by authentication key and lack of fine-grained access control. The first one, overload by authentication key, happens when all the components in HDFS share the same symmetric authentication key. If this occurs, compromising any single node would result in compromising all the data in all the HDFS components. The other vulnerability, the lack of fine-grained access control, can be traced back to lacks in Delegation Tokens: currently a Delegation Token authorizes a process to access the data of a user, but it doesn't distinguish different parts of the user's data, an attacker that intercepts a Delegation Token can impersonate the user and have access to her entire data set.

Xianqing Yu et al. propose a new model that uses Block Token and a new Delegation Token, to solve this problems and enhance security of Hadoop deployments in cloud.

\paragraph{A new solution of data security accessing for Hadoop based on CP-ABE} % Zhou2014
Huixiang Zhou and Qiaoyan Wen present a new security access control solution for Hadoop based on CP-ABE \cite{zhou2014new}. This solution is based in two main parts, Authorization Management, which manages the attributes of the users and Authorization Control, which manages the policies. This separation of powers is designed to keep the user's privacy as the resource provider doesn't have to have access to all the user's information in order to decide whether a she has access to a file or not.

In the other hand, the solution provided by Zhou et al. is an attribute based access control system that works in an application level, but delegates the encryption to HDFS instead of applying ABE.

\paragraph{Data Security Accessing for Hadoop based on
Masked CP-ABE} % Gaur 2015
After reviewing the work proposed by Zhou et al. in \cite{zhou2014new}, Suparna Gaur \cite{Gaur2015DataSecurity} thinks that this work has a issue: The base researches don't specify the types of attributes the users and the files need to be given nor define the access policy to be applied over the encrypted data. To solve this issue, he provides a new encryption scheme called Masked CP-ABE (M-CP-ABE) that extends the base CP-ABE algorithm by specifying the types of attributes and access policies to be applied over the encrypted data in form of masking.

T-CP-ABE gives a mask to each file, then, when decrypting a file, this mask and the user's key are compared to get a match factor, if the factor is bigger than a threshold, the file will be decrypted.

This scheme provides more flexibility to the previous models and can be applied to provide an easier implementation of more complex access structures. This is a big improvement for the Hadoop attribute based access control solutions, but it can be used in any other application for ABE. 

\paragraph{Object-Tagged RBAC Model for the Apache Hadoop Ecosystem} % Gupta 2017
Maanak Gupta et al. \cite{gupta2017object} present an access control model for Hadoop called HeAC as an improvement to the current Apache Ranger's \cite{apacheranger} user and group permission system and Apache Sentry's \cite{apachesentry} role based permission system. HeAC expands in this systems including attributes (tags) that can be assigned to any object in the Hadoop ecosystem depending on their sensitivity, content type or expiration date.

After presenting HeAC, Gupta et al. extend their model proposing Object-Tagged Role-Based Access Control (OT-RBAC), a model consistent with the widely accepted RBAC definitions which adds object attributes to the role based permission system like in HeAC. OT-RBAC can be expanded to provide a fine-grained access policies combining attributes and roles.

\paragraph{An Attribute-Based Access Control Model for Secure Big Data Processing in Hadoop Ecosystem} % Gupta2018
Maanak Gupta et al. propose a attribute-based access control model for Hadoop called HeABAC \cite{Gupta:2018:AAC:3180457.3180463}. Continuing their previous work \cite{gupta2017object}, they propose another extension to HeAC, this time adding user attributes and allowing to specify policies for different services and operations. This way, only users with certain attributes will be able to interact with certain services in the Hadoop ecosystem.

Although this work presents a robust attribute based access control, it doesn't include any HDFS and DataNode level security, Gupta et al. point out that this could be a future extension to the model.