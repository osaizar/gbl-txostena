\chapter{Results}\label{ch:chapter 3}

After running the tests, we have a lot of data that needs to be interpreted. To ease this task we used Python notebooks in Jupyter \cite{jupyternotebook}. The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text and it's widely used in data mining, because of that, we used it to handle our datasets and create graphics that help in the interpretation of the results.

In this chapter we will show the results of our tests and try to analyze them. First we will analyze the tests made with simple policies and then, after drawing some conclusions, we will analyze the numeric policies.

If you want more detailed information, all the datasets can be found here \cite{resultscsv} and the Jupyter notebooks can be found in the appendix \ref{appendix:jupyter-notebooks}.

\section{Simple Policies}
We made two tests with simple policies \ref{tab:testtable}, \textit{Policy Length}, where we incremented the length of the policy each iteration and \textit{Policy Number}, where we added a new policy with the operand \textit{and} each iteration. In this section we will show the results of those tests with two objectives in mind, the impact in the performance of the policies and the length of the created EDEKs.

\subsection{Policy Number}
In this section we will analyze the results of the \textit{Policy Number} test first by looking into the performance measurements and then into the EDEK lengths.

\subsubsection{Performance}
As seen in the data, the time needed to \textbf{create a key} \ref{fig:PNcreatekey} keeps a constant value throughout the test, only peeking in the first iteration. This is a tendency that continues to happen throughout all the tests so we don't attribute this change to the single policy test, instead, we think that this happens simply because it's the first created key.

The performance change of the other measured actions is remarkable: the time needed to \textbf{create a zone} \ref{fig:PNcreatezone}, \textbf{put a file} \ref{fig:PNinsertfile} and \textbf{read a file} \ref{fig:PNgetfile} rise linearly as the number of policies increases, however, \textbf{create a zone} increases faster than the other two because of the Warm Up process \ref{ss:createzone}.

\subsubsection{EDEK size}
As seen in the graphics, the size of the EDEKs increases linearly too. A linear regression reveals that the slope of the graph is of 274 bytes per added policy. 

There is a exception to this rule: the first value, where it's difference to the second one is of 282 bytes (274+8). Further testing has shown that this happens because it's the first time where the \textit{and} operand is used.

\subsection{Policy Length}
In this section we will interpret the results of the \textit{Policy Length} test. As in the previous section, first we will talk about the performance measurements and their meaning and then about the EDEK sizes.

\subsubsection{Performance}
The data shows that there is very little or no variation in all the measured actions while increasing the length of the policy.

The real change happens when we compare the tests where only one policy was used and the tests where five policies were used. All the measured data seems to go up, the most noticeable difference happening in the \textbf{create zone} action. This data coincides with our conclusion in the \textit{Policy Number} tests.

\subsubsection{EDEK size}
The increase of the size of the EDEKs is fully linear in this case. A linear regression shows that the slope is 1 byte per character in the case of one policy and 5 bytes per character in the case of 5 policies.

It looks like the size of the EDEK increases a byte for each character in the policy. This is proven by the fact that the slope is 1 in the first case and 5 in the last one, as that's the number of characters added to the policies each iteration.

\section{Conclusions in simple policies}
After looking to the \textit{Policy Number} and \textit{Policy Length} tests, we have a better understanding of how simple policies affect our system. The results seem consistent and we can draw some conclusions from them.

To start with, we know that increasing the length of each policy increases the length of the EDEK linearly and that adding a policy increases the length by 273 plus the length of the new policy. 

Looking at the data we can make some more conclusions: the size of the EDEKs with one policy is of 581 bytes plus the size of the policy, so if each policy adds 273 bytes:

\[
  {base} = 581 - 273 = \textbf{308}
\]

This \textit{base} will be the hypothetical size of a EDEK with no policies, we need to start adding all the extras to that number.

Another problem with simple policies is that the \textit{and} and \textit{or} operands seem to add 8 bytes to the EDEK but only some times. Further testing has revealed that \textit{and} operand \textit{sets} add 8 bytes to the EDEK and another 8 bytes are added if there is a \textit{or} operand in the policy.

Knowing this information we can start trying to predict the length of an EDEK depending on the policy that we use to encrypt it: this formula can predict the length of an EDEK encrypted with simple policies:

\[
{edek\ length}=308+8m+8r+\sum_{i=1}^{l_a} (273+i){L_i}
\]

Where ${l_a}$ is the length of the longest policy, ${L_i}$ is the number of policies with the length $i$, $m$ is the number of $and$ operand sets and $r$ is equal to $1$ if there is any $or$ operands and $0$ if there is not.

We implemented this formula in a Python script, it can be found in the appendix \ref{pdf:simplepolicies}.

\section{Numeric Policies}
After reviewing the simple policies, we can start to analyze the numeric policies. We made six different tests involving numeric policies, but we will not analyze the results of all of them because we found them inconclusive. \gorriz{errebisatu}

\subsection{Numeric 1}
In this section we will analyze the results of the \textit{Numeric 1} test.

\subsubsection{Performance}\label{ss:n1performance}
The data shows that the time to \textbf{create a key}, \textbf{insert a file} and \textbf{get the file} remain constant while increasing the value of n, both in < and in >.

The time recorded while \textbf{creating a zone} show a different result. The graph shows that the performance varies greatly but we can distinguish three different intervals where the max values remain constant. 

\subsubsection{EDEK Length}
The graphs show us that the EDEK length follows the same intervals mentioned in the performance section \ref{ss:n1performance}.

After further analyzing the data we know that the N values where the EDEK lengths and the create zone performance make this great changes with the < sign are \textit{2\textsuperscript{1}, 2\textsuperscript{2}, 2\textsuperscript{4} and 2\textsuperscript{8}} and with the > sign: \textit{2\textsuperscript{1}-1, 2\textsuperscript{2}-1, 2\textsuperscript{4}-1 and 2\textsuperscript{8}-1}

It only makes sense to the next values to be 2\textsuperscript{16} and 2\textsuperscript{16}-1, this was confirmed with further testing in the appendix \ref{pdf:numericpolicesip}.

To conclude, despite being the simplest numeric policy test,  we don't have any explanations of why this EDEK length changes happen and we are not able to predict the length of an EDEK based in the N value. The only conclusion that we can get is that the length of the EDEKs has a direct influence on the performance of the create zone command.

\subsection{Numeric 2}

\subsubsection{Performance}
All the recorded values remain constant or with no major changes.

\subsubsection{EDEK Length}
As seen in the graphics, increasing the length of the numeric policies increases linearly the length of the EDEKs. This happens differently in < and in >: while the slope of < is 7 bytes per character and the starting value is 2428 bytes, the slope of > is 6 bytes per character and the starting value is of 2073 bytes.

More testing is necessary to see if the length of the policy has the same effect in the EDEK's length with different N values.