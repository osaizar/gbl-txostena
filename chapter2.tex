\chapter{Project Development}\label{ch:chapter 2}
\section{Research and Preparation}
Before starting with the design or development of this project, we had to prepare a working environment and understand how the tools we were going to use worked. Because of that, in this section we will show the learning phase of this project.

This project continues the work of Aitor Osa et al. in \cite{Osa2017Cifrado}, and some previous research made by Oier Saizar and Mikel Amuchastegui \cite{amutxas2015} in 2016. Those works helped greatly in the learning phase of this project as some guides and documents were written to ease the continuing of this work. Thereby, in this section we will make some references to this documents.

\subsection{CP-ABE and CP-ABE Toolkit}
As explained earlier in the document, CP-ABE is an Attribute Based encryption algorithm proposed by Bethencourt et al. in \cite{bethencourt2007ciphertext}. Before starting to work in this project this types of algorithms were unknown to us, so in the learning phase of the project we had to install the CP-ABE Toolkit and familiarize with it. After making some tests and creating different access structures we got a better understanding of the algorithm.

\subsection{Development environment set up}
Before trying to debug and edit Hadoop's source code, it was necessary to create a development environment that could run and compile Hadoop. To do so, we followed the guide written by Mikel Amuchastegui \cite{amutxas2015} explaining how to use Apache Maven \cite{apachemaven} to setup a project for the Eclipse IDE \cite{eclipse} and how to configure Eclipse to run and compile different parts of Hadoop.

We had two main problems with this development environment: The first one is that Amuchastegui doesn't explain how to run the Hadoop KMS using Eclipse, so after making some research we decided that the best option was to run the KMS manually from the terminal using the \textit{kms.sh} script. The other one was that we found the process of compiling the code using Eclipse unreliable as some parts of the code wouldn't be compiled always, so we started using Maven to compile the code from the terminal.

\subsection{Hadoop Code Documentation}
In order to design a secure architecture, a deep understanding of how HDFS and Its encryption zones worked was needed. In this phase, we documented and understood all the relevant classes and functions in Hadoop, writing a document explaining all our findings in the process as seen in the appendix \ref{appendix:hadoop-source}. This phase was one of the most time consuming tasks of this project.

Hadoop is formed by different components and many lines of code, as we didn't need to understand all the functions and classes, we divided our research in 4 different functionalities that needed to be analyzed.

\begin{itemize}
	\item Create Key
	\item Create Zone
	\item Put File
	\item Get File (cat)
\end{itemize}

It's very important to understand that these functionalities do not all happen in a single Hadoop module, so to fully understand them we needed to debug the code from the Hadoop NameNode (\ref{p:NameNode}), HDFS \ref{p:HDFS} and the KMS \ref{p:kms}.

\subsubsection{Create Key}
In order to create a encryption zone we need a key that is going to be used to encrypt the DEKs of
the files stored in the zone. This is the command that is used to create the key:

\begin{lstlisting}[language=bash]
  $ hadoop key create <keyName> [-cipher] [-size]
\end{lstlisting}

This command takes at least one argument, the key name. The name will be used as an identifier to reference the key by the KMS and by the users that want to create a zone. The other arguments are optional in case we want to create a key using a specific cipher and make sure that its size is the correct one, if not specified, the default algorithm is \textit{Advanced Encryption Standard} (AES) with no padding.

\begin{landscape}
  \begin{figure}[p!]
    \centering
    \includegraphics[width=\linewidth]{CreateKey.png}
    \caption{\textit{Create Key} sequence diagram}
    \label{fig:CreateKeySeq}
  \end{figure}
\end{landscape}

The diagram in the figure \ref{fig:CreateKeySeq} shows that first the command is parsed by the hadoop-common client and a JSON request is forged with the information of the key to be created and sent to the KMS. The KMS takes the request and generates a key with the given cipher, checks if the key name exists and if not it stores the key with the given name. After storing the new key, the KMS sends an OK response with the new key's version.

\subsubsection{Create Zone}\label{ss:createzone}
The create zone command associates an existing directory with a key to create a encryption zone, in an encryption zone all the files are encrypted using DEK keys and all the DEK keys are encrypted using the key that the user chooses. The command takes two arguments, the key name of the key that it's going to be used and the path of the encryption zone:

\begin{lstlisting}[language=bash]
  $ hdfs crypto -createZone -keyName <keyName> -path <path>
\end{lstlisting}

\begin{landscape}
  \begin{figure}[p!]
    \centering
    \includegraphics[width=\linewidth]{CreateZone.png}
    \caption{\textit{Create Zone} sequence diagram}
    \label{fig:CreateZoneSeq}
  \end{figure}
\end{landscape}
As seen in the sequence diagram of the figure \ref{fig:CreateZoneSeq}, when creating an encryption zone, HDFS takes the command and sends it to a NameNode, then the NameNode adds the zone to a list and returns an OK response. After doing this, a process called Warm Up starts in the NameNode, and creates and encrypts 1000 DEKs to use them when needed. If the NameNode is restarted, it needs to Warm Up all it's encryption zones and store the DEKs in a cache.

\subsubsection{Put File}
This command is used each time the user needs to upload a file to HDFS. If the path is an encryption zone, the file is encrypted with an DEK. The command takes two arguments, the path of the file that is going to be uploaded and the path of the folder where it's going to be uploaded:

\begin{lstlisting}[language=bash]
  $ hdfs dfs -put <file> <path>
\end{lstlisting}

\begin{landscape}
  \begin{figure}[p!]
    \centering
    \includegraphics[width=\linewidth]{PutFile.png}
    \caption{\textit{Put File} sequence diagram}
    \label{fig:PutFileSeq}
  \end{figure}
\end{landscape}

The sequence diagram in the figure \ref{fig:PutFileSeq} shows that the Put File process has two different parts. First, the HDFS client needs to get an EDEK of the encryption zone that the file will be stored in, to get it, it sends a request to the NameNode. The NameNode checks if it has any EDEKs stored in the cache, if so it returns the EDEK, if not, it generates a new EDEK and sends it back to the HDFS client.

After getting the EDEK, the client needs to decrypt it in order to use it, so it sends a request to the KMS to decrypt it. After getting the DEK, the file is encrypted with it and stored in the distributed file system. The EDEK is stored in it's metadata so to be used in the file decryption.

\subsubsection{Get File}
The \textit{cat} command runs a Unix utility that reads the content of the file specified in the argument and prints it in terminal. We use \textit{cat} because It's one of the simplest actions that imply decrypting a file if it's in a encryption zone. The command takes one argument, the path of the file that the user wants to read.

\begin{lstlisting}[language=bash]
  $ hdfs dfs -cat <filePath>
\end{lstlisting}

\begin{landscape}
  \begin{figure}[p!]
    \centering
    \includegraphics[width=\linewidth]{GetFile(cat).png}
    \caption{\textit{Get File} sequence diagram}
    \label{fig:GetFileSeq}
  \end{figure}
\end{landscape}

As seen in the sequence diagram of the figure \ref{fig:GetFileSeq}, when reading an encrypted file, the HDFS client gets the encrypted file and extracts the EDEK from its metadata and the key version of the key it was encrypted with, then, the EDEK is sent to the KMS to be decrypted. The KMS decrypts the EDEK with the key and returns the DEK, now HDFS can proceed to decrypt the file and to run \textit{cat} command showing its content. We can see that the decryption of the EDEK in Get File follows the same process as in Put File, in both cases, the KMS is needed to get the DEK and continue with the file encryption or decryption.


\subsubsection{General Structure}
After reviewing these 4 functions, we can sum up the Hadoop encryption zone security architecture with the model found in the figure \ref{fig:HadoopEZModel}.

\begin{itemize}
  \item Create Key creates a key with a given name in the KMS.
  \item Create Zone associates a key with a given folder and creates 1000 EDEKs.
  \item Put File has two steps:
    \begin{itemize}
      \item Get a EDEK from the cache or create a DEK and encrypt it with the zone key.
      \item Decrypt the EDEK and use the created DEK to encrypt the file and store the EDEK in the files metadata
    \end{itemize}
  \item Get File gets the files EDEK from the metadata, decrypts it and uses it to decrypt the file.
\end{itemize}

The access to the encryption zones can be controlled in two ways: on one hand, HDFS supports Unix like user and group permissions, so the administrator can give or revoke permission to read or write in a folder to any user in the system. In the other, the KMS has ACLs that can manage the permissions that the users have over the keys. This ACLs can be used to give administrative power to certain users, allowing them to create and delete keys, but they can also be used to to deny a user the permission to use a certain key, revoking her the access to all the EZs that use it.

\begin{figure}
  \includegraphics[width=\linewidth]{CryptoDiagramaAES.png}
  \caption{Hadoop EZ model}
  \label{fig:HadoopEZModel}
\end{figure}

\section{Design}
After analyzing the behavior of Hadoop EZs, we developed security architecture introducing CP-ABE to HDFS. Different designs were created in the process of finding the most suitable one for the project, in this section we will show two preliminary designs and their advantages and disadvantages. After that, we will present the final design and discuss why it's the best approach.

\subsection{Preliminary Designs}
This section presents the early designs of the architecture, then, it compares the advantages and disadvantages of the approaches and explains the reason why they haven't been used.

\subsubsection{Policy Per Zone}
This was our first approach to the problem, the diagram of the model can be found in the figure \ref{fig:HadoopPolZv1}. In this approach when \textbf{creating a key}, a policy is passed as an argument. The KMS will store this policy as the key of the zone. With the \textbf{create zone} command, a given folder is associated with a key, just like in the traditional Hadoop version. The \textbf{Put File} command has two steps: A DEK is created and the file is encrypted with it using AES. Then the DEK is encrypted using the CP-ABE Public Parameters of the system and the policy of the zone, creating an EDEK that only can be decrypted with a key that satisfies the zone policy. \textbf{Get File} also happens in two steps: first the EDEK is extracted from the files metadata, to decrypt the EDEK a Private Key is created with the users attributes and the Public Parameters and Master Key of the system. If the attributes of the user satisfy the policies of the zone, the private key will be able to decrypt the EDEK. Finally, the DEK is used to decrypt the file with AES.

\begin{figure}
  \includegraphics[width=\linewidth]{CryptoDiagramaCPABEPolZoneV1.png}
  \caption{Hadoop CPABE Policy Per Zone v1}
  \label{fig:HadoopPolZv1}
\end{figure}

\subsubsection{Master Key Per Zone}
This is the second approach, it changes how the Public Parameters and Master Key are handled in the system, the diagram can be found in the figure \ref{fig:HadoopMasterKeyZ}. This approach differs from the previous one in that policies are not associated with the encryption zones, instead, we associate a Master Key and the Public Parameters to each EZ of the system. So, \textbf{creating a key} generates a Master Key and a Public Parameters and stores them in the KMS, then, \textbf{creating a zone} associates these keys with the zone. The policy of the file is specified as an argument of the \textbf{put file} command. A DEK is generated, the file gets encrypted with the DEK and the DEK is encrypted with the policy and the PK of the EZ. When \textbf{getting the file} the decryption process happens the same way as in the previous model: A key is created with the attributes of the user and the keys of the EZ, if the attributes satisfy the policy of the file the user key will be able to decrypt the EDEK and get the key to decrypt the file.

\begin{figure}
  \includegraphics[width=\linewidth]{CryptoDiagramaCPABEMasterZone.png}
  \caption{Hadoop CPABE Master Key Per Zone}
  \label{fig:HadoopMasterKeyZ}
\end{figure}

\subsubsection{Advantages and disadvantages}
The main difference of these two designs is in where the file policies are defined. In the first proposed design, each EZ has its own policy and to create a new policy it's necessary to create a key and the associate a EZ to the key. In the other side, \textit{Master Key Per Zone} makes the creation of new policies much easier as the policy is not defined until the file is uploaded to the system. Furthermore, the EZs serve to the purpose of separating different MKs and PKs so that even if a user has the attributes to decrypt a file it won't be able to do so if it doesn't have the permissions to access that EZ.

At first glance it would seem that the last design is superior because it's more flexible and can offer a better granularity, but there is a problem with both of the designs: because of an error in the research phase, we didn't take the Warm Up process that happens in the NameNode after the zone creation into account. This process creates and caches all the EDEKs when the zone is created, so the EDEK is ready when file is uploaded to the system. This means that to encrypt a file first a EDEK needs to be decrypted, not the other way around.

This error makes both the approaches impossible to apply without some changes. But although \textit{Policy Per Zone} can be edited to take the new discovery into account (with some compromises), \textit{Master Key Per Zone} can not. The base of this design is to encrypt the DEKs with the policy given by the user in the Put File command, as the DEKs need to be encrypted beforehand it's impossible to do so without making great changes in the Hadoop source code.

Taking this into account, the chosen design is a modified version of \textit{Policy Per Zone}.

\subsection{Final Design: Policy Per Zone v2}\label{ss:policyperzone}
This is the third and last approach, it's similar to \textit{Attributes Per Zone v1} but it solves its problems. The diagram of this approach can be found in the figure \ref{fig:HadoopPolZv2}.

The same way as in the first version, when \textbf{creating a key}, a policy is stored in the KMS as a key ready to be used in a zone. \textbf{Creating a zone} associates a folder with the policy that will be used to encrypt the DEKs of the files that will be saved in it. To \textbf{get the file} the user has to be able to decrypt the EDEK with his attributes.

On the other hand, there are some differences too: As the DEKs are created and encrypted after the zone is created, encrypting a file requires to decrypt an EDEK to use the DEK. Because of that, to \textbf{put a file} the user also needs to be able to decrypt the EDEK, therefore, the attributes of an user need to fulfill the policy of a EZ in order to be able to write in it. This is a limitation of the design, but we decided to accept it as it's not a big concern for our objectives.

In all the different approaches that we can think of the KMS needs to be in a secure server, so it only makes sense to store the list of attributes of each user in the same server.

\begin{figure}
  \includegraphics[width=\linewidth]{CryptoDiagramaCPABEPolZoneV2.png}
  \caption{Hadoop CPABE Policy Per Zone v2}
  \label{fig:HadoopPolZv2}
\end{figure}

To conclude with, the chosen design allows for a granular control of the access to the encryption zones associating each zone with a policy and requiring the users to fulfill the policies of the zones in order to access them. The attributes of the users will be stored in the KMS and there will only be a MK and a PK in the cluster.

\section{Development}\label{s:development}
After the design phase we are ready to start to develop our model. In this section we will explain the different steps taken to do so. Finally, we will conclude by showing how to compile this modified version of hadoop into binaries so it can be deployed in a server. All the source code can be found in \cite{cpabe4hadoop}

\subsection{Adding DES to Hadoop} \label{ss:destohadoop}
In order to check the complexity of modifying the encryption algorithm in Hadoop, we started by changing the encryption algorithm to another Symmetric-key algorithm. The algorithm chosen was \textit{Data Encryption Standard} (DES), a well known cipher that is not currently in use because its weakness to brute-force attacks. This modification was only implemented with research purposes, as DES should not be trusted as a secure cipher now a days.

Thanks to the previous research by Mikel Amuchastegui \cite{amutxas2015} we were able to add DES into Hadoop following his documentation. Hadoop is designed to allow an easy implementation of different encryption algorithms with only basic changes in the code, without going into details, in order to add any encryption algorithm to Hadoop we need to implement a Java \textit{CryptoCodec} class. This class will include decrypt and encrypt methods in which the algorithm can be applied. For example, for our DES test, we will need to create a class called \textit{DesCtrCryptoCodec} that uses DES to encrypt and decrypt the \textit{ByteBuffers} that are sent to it. Hadoop also needs to be specified which algorithms are available to use in a \textit{enum} that holds all the ciphers. All the steps on how to change the algorithm to DES can be found in the appendix \ref{appendix:hadoop-chalgorithm}.

After this, we can start using the newly added cipher in Hadoop just by using the \textit{-cipher} option in the create key command:

\begin{lstlisting}[language=bash]
  $ hadoop key create <keyName> -cipher DES
\end{lstlisting}

Once added DES in Hadoop, we can follow a similar process to add CP-ABE, yet, as mentioned earlier, CP-ABE is not a Symmetric-key algorithm, so further changes are needed to add this algorithm into Hadoop.

\subsection{CpabeDriver}
To create the encryption keys and encrypt files with AES and DES Hadoop uses a Java class included in the \textit{Javax} library called \textit{KeyGenerator}, however we can't use the same class for CP-ABE as this algorithm is not included. To solve this problem we created the \textit{CpabeDriver} class, a Java interface for the \textit{CP-ABE Toolkit}. As mentioned earlier this toolkit is a C program shown by Bethencourt et al. in \cite{cpabetoolkit}, \textit{CpabeDriver} requires the \textit{CP-ABE Toolkit} to be installed in the system so it can use Bash to call its different functions. This way, we can integrate CP-ABE without having to fully port the toolkit to Java.

\subsection{Adding CP-ABE to Hadoop}\label{ss:cpabetohadoop}

After implementing the \textit{CpabeDriver}, we can follow the same steps as in the DES implementation (\ref{ss:destohadoop}) to add CP-ABE support to Hadoop. The main difference will be the \textit{CpabeCryptoCodec}, the other \textit{CryptoCodecs} are designed to use Symmetric-Key algorithms, so our codec will have some differences:

\paragraph{The constructor} At start, the object needs to load the Master Key and the Public Parameters of the system, first getting the location of the files form the configuration and then loading the contents of the files.

\paragraph{Init} The init function loads the key as in the other algorithms, but in this case, the loaded key is stored as the policy of the EZ and will only be used in the encrypt function.

\paragraph{Encrypt} \textit{CpabeDriver} is used to encrypt the DEK using the given policy. There is another problem that needs to be fixed: in CP-ABE the EDEKs can get very large and vary greatly, and as the encrypt function uses \textit{ByteBuffers} that require to know the length of the EDEK before encrypting it, it needs to be rewritten. Without this change, the \textit{ByteBuffers} can overflow if the policy is too complex.

\paragraph{SetAttributes} This is a simple setter function that needs to be added in order to obtain the user attributes that are needed to decrypt the file.

\paragraph{Decrypt} The same way as in the encrypt function, \textit{CpabeDriver} is used to create a key with the given user attributes and then attempt to decrypt the file using the created key. If successful, the decrypted DEK is returned.s

\subsection{Add the policy argument}\label{ss:addpolicyargument}
To continue with our task, we need to add a way to allow the users to specify the policy for each key. The most intuitive way to do this is adding a \textit{-policy} option in the key creation command. This option was added editing the class responsible for parsing the \textit{create key} command and adding a "policy" variable in the \textit{Options} class. This is the class that manages the different options of the keys that need to be created in the KMS. Furthermore, the KMS needs to be edited too so instead of generating a key, it can save the policy as the key if the key that's been created is a CP-ABE key.

With this changes, policies can be specified like this:

\begin{lstlisting}[language=bash]
  $ hadoop key create <keyName> -cipher CPABE -policy <policy>
\end{lstlisting}

\subsection{CP-ABE and AES}
As seen in the Design phase and in the figure \ref{fig:HadoopPolZv2}, in our system, CP-ABE is not used to directly encrypt files, instead, the files are encrypted using AES like in the traditional Hadoop version and the AES DEKs are encrypted with CP-ABE.

This happens because the size of the CP-ABE encrypted files can be very large depending on the policy used to encrypt them. So, whereas in AES or DES we only have to take one encryption algorithm into account, in our design both CP-ABE and AES are used. In order to make this possible, the DEKs generated need to be valid AES keys and the algorithm needs to be changed from CP-ABE to AES when encrypting files.

This section explains the changes made to create valid DEKs and to use different algorithms to encrypt files and DEKs.

\subsubsection{DEK creation}
This is the most simple part, the DEKs are created taking the block size of the algorithm that we choose to use into account. This block size gets specified in the \textit{enum} mentioned in the DES test section (\ref{ss:destohadoop}) together with the algorithm name. Using the same block size as AES for CP-ABE guarantees that all the DEKs that will be created will work as AES keys.

\subsubsection{CP-ABE to AES transition}
For the sake of transitioning from CP-ABE to AES when necessary, we need to know where the files are encrypted and decrypted opposed to where the DEKs are encrypted and decrypted. Thanks to our previous research we know that while the DEKs are handled by the KMS, the files are encrypted and decrypted in HDFS, because of that, by forcing the use of AES in HDFS we can use CP-ABE in one side and AES in the other, this way we can have the advantages of fine-grained encryption of CP-ABE without its downsides.

To do so, we edited the function \textit{getCryptoCodec} in \textit{DFSClient} to return AES if asked for CP-ABE.

\subsubsection{EDEK size with CP-ABE}
One of the problems that we encountered in the development of this project were the overflows created in some buffers while encrypting the DEKs using CP-ABE. This overflows happen because of two reasons: in one side, the EDEKs encrypted with CP-ABE are much larger than the ones encrypted with other algorithms as AES or DES and in the other, the length of the EDEK keys is not predictable, as it changes with the policy that it was encrypted with.

At first, we thought we could find some way to predict the size that an EDEK would have knowing the size of the DEK and the policies that were used to encrypt it, but tests (\ref{s:testing}) showed that the EDEK length varies greatly in an unpredictable way with some sets of policies.

The only remaining solution was to get the size of the EDEK after encrypting it and creating the buffers knowing the exact size. So we edited the code to allow this solution.

\subsection{Attribute storage} \label{ss:kmsattributes}
The attributes of the users are the base of our fine-grained access control system, therefore, they need to be protected in a secure server that is only accessed by the administrators of the Hadoop cluster. In our design, the most secure server needs to be the KMS as an attacker with access to it could have access to all the keys of the cluster. Because of that, the attributes should also be stored in the KMS.

There are many ways to store these attributes securely in the KMS and our approach is not the best one. In our design the attributes are stored in a text file resembling the Unix \textit{/etc/passwd} file \cite{unixpasswd}, where each user is listed in one line followed by her attributes.

The user attribute file has the next format:

\begin{lstlisting}
alice::access_level = 3, department_it
bob::access_level = 1, department_finanances, afternoon
\end{lstlisting}

In the future, given more time, we should find a way to encrypt (or maybe hash) the contents of this file. Another way to store this data securely would be to use a \textit{Java Key Store} \cite{javakeystore} instead.

\section{Testing}\label{s:testing}
In the development phase \ref{s:development} of this project we had some problems regarding the size of the EDEKs that were encrypted using CP-ABE, we realized that while encrypting 128 bit long DEKs with AES would result in a EDEK of 144 bytes every time, encrypting DEKs of the same length with CP-ABE results in EDEKs of various lengths depending in the policy that it was encrypted with.

For example, a if we encrypt a DEK with the policy \textit{"department-it"} the EDEK will be 594 bytes long, but if we encrypt a DEK with the policy \textit{"access-level > 5"} the resulting EDEK will be 2578 bytes long.

This inconsistency forced us to use dynamic buffers in the encrypt function of our implementation and made us think that this matter needed further investigation.

Because of that, in this phase we tested our Hadoop system with different policies and file lengths and recorded different performance measurements and the lengths of the EDEKs with the aim to benchmark our system and analyze the patterns of EDEK lengths. This section of the document explains the testing phase of our project: how the tests were designed and how they were executed. The results are shown in the next chapter (\ref{ch:chapter 3}).

\subsection{Test design}\label{ss:testdesign}
Before starting with the tests, it's important to specify what data will be recorded and which tests need to be run in order to get data sets that are relevant for our investigation. As mentioned, in our case, the objective of this tests is to benchmark the performance of our system and to look for patterns in the length of the EDEKs. So the tests need to be designed with this objective in mind.

The best way to benchmark all the functions we implemented in our system would be to complete all the process that a file needs to follow to be encrypted: Create a key with a policy, create a zone with the key, put a file in the zone and read the file. Following this cycle we can record the time it takes to run each command and read the EDEK of each encrypted file. This way, we will see which command varies with different inputs that we can provide. The variables that we will record can be found in \ref{tab:outputvariables}.

\begin{center}
    \begin{longtable}{ | l | p{7cm} |}
    \hline
    \bf Variables & \bf Description \\ \hline
    create\_key\_time & Time needed to create a key \\ \hline
    create\_zone\_time & Time needed to create a zone \\ \hline
    insert\_file\_time & Time needed to upload a file \\ \hline
    get\_file\_time & Time needed to read a file \\ \hline
    edek\_size & Length of the EDEK \\ \hline
    \caption {Table of the output variables}
    \label{tab:outputvariables}
    \end{longtable}
\end{center}

\begin{center}
    \begin{longtable}{ | l | p{7cm} |}
    \hline
    \bf Variables & \bf Description \\ \hline
    policy & policy used to create a key \\ \hline
    in\_file\_size & Size of the input file \\ \hline
    \caption {Table of the input variables}
    \label{tab:inputvariables}
    \end{longtable}
\end{center}

Once decided how we will perform and record the benchmarks, we need to define the input variables that we will control to differentiate the tests. Between all the inputs that we control we think that the two most important ones are the EZ policy and the file that needs to be encrypted, however, taking into account the impact of the input file in some of the processes (like the key creation or the zone creation) should be non-existent and the vast number of policy types that need testing, we decided that most of the test will be focused in the policy variable, using a random 128 byte file as an input. The input variables can be found in the table \ref{tab:inputvariables}

In conclusion, taking the different kinds of policies that can be tested into account   (previously mentioned in \ref{sss:cpabepolicytypes}), we decided to run the tests found in the table \ref{tab:testtable}

\begin{center}
    \begin{longtable}{ | l | p{9cm} | p{2cm} |}
    \hline
    \bf Test name & \bf Description & \bf Example \\ \hline

    Policy Length & Starting from a simple policy of a character, add a character to the policy each iteration for 200 iterations. Repeat with two, three, four and five policies. & \textit{a, aa, aaa...} \\ \hline

    Policy Number & Starting from a simple policy of a character, add one policy with the operand \textit{and} per iteration until 150 iterations. & \textit{a, a and a, a and a and a...} \\ \hline

    Numeric 1 & Starting from a single character numeric policy where \textit{n = 0}, add one to \textit{n} each iteration for 500 iterations. Twice, one for < and the other one for > & \textit{a, aaaa, aaa... a and a, aa and aa...} \\ \hline

    Numeric 2 & Starting from a single character numeric policy where \textit{n = 1}, add one character to the policy each iteration for 500 iterations. Twice, one for < and the other one for >. & \textit{a < 1, aa < 1, aaa < 1...} \\ \hline

    Combined 1 & Starting from a single character numeric policy where \textit{n = 0} and a single character simple policy, add one to \textit{n} each iteration for 300 iterations. Twice, one for < and the other one for >. & \textit{a < 1 and b, a < 2 and b, a < 3 and b...} \\ \hline

    Combined 2 & Starting from a single character numeric policy where \textit{n = 0} and  two single character simple policies, add one to \textit{n} each iteration for 300 iterations. Twice, one for < and the other one for >. &  \textit{a < 1 and b and c, a < 2 and b and c, a < 3 and b and c...} \\ \hline

    Combined 3 & Starting from two single character numeric policies where \textit{n = 0} and \textit{m = 1}, add one to \textit{n} each iteration for 300 iterations. Twice, one for < and the other one for >, only changing the first numeric policies sign. & \textit{a < 1 and b < 1, a < 2 and b < 1, a < 3 and b < 1} \\ \hline

    Combined 4 & Starting from two single character numeric policies where \textit{n = 0} and \textit{m = 0}, add one to \textit{n} and one to \textit{m} each iteration for 300 iterations. Twice, one for < and the other one for >, only changing the first numeric policies sign. & \textit{a < 1 and b < 1, a < 2 and b < 2, a < 3 and b < 3} \\ \hline

    File Size 1 & Using a constant numeric policy of one character and \textit{n = 1} and a starting input file size of 100 bytes, add 100 bytes to the input file for 1000 iterations. & \textit{n < 1} \\ \hline

    \caption {Table of the designed tests}
    \label{tab:testtable}
    \end{longtable}
\end{center}

\subsection{Test execution}
In order to run the tests in a reliable way, we need to setup a fail-proof environment with the sufficient computing power to run all the tests in a reasonable amount of time. To do that and taking into account that the tests can't be run in parallel in the same machine, we decided to user a server of the university to create 7 Virtual Machines with \textbf{3 CPU cores and 8gb-s of RAM} each. This way, 7 tests can run at the same time in different machines independently. In order to maintain statistic validity, all the tests were executed at least 30 times, reseting the Hadoop NameNode, DataNode and the KMS at the start of every execution.

Running this tests manually would be impossible, so instead, we wrote a small Python script that can be configured to run any tests in our system. With some configuration, this script starts the Hadoop server, creates and uploads files with the length of our choosing and creates keys and zones with the required policies. Each time a test set ends, the script writes the results in a CSV file, resets the server and starts with the next one. The script can be found here \cite{testscript}.

The execution of all the tests had the servers running for more than 20 days, as some tests took much more time to execute than expected.