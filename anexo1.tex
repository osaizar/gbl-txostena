\hypertarget{appendix1}{\chapter{Hadoop: Change the encryption algorithm}}
\label{appendix:hadoop-chalgorithm}

\section{Introduction}
The default algorithm of the transparent encryption of HDFS is \textit{AES/CTR/NoPadding}, a symmetric-key algorithm that uses 128 bit keys by default.

In this document we will explain how to change the algorithm to another symmetric-key algorithm, how to compile the code and finally, how to run it.

This document combines the work by Mikel Amutxastegi \cite{amutxas2015} and our experience to provide a better guide for the process.
\section{Changes in the code}

HDFS is prepared to work with symmetric-key algorithms so the change just involves little adjustments in the code. In the example the algorithm used is DES, an older algorithm that uses 56 bit keys. DES is not as safe as AES, so this modification is only recommended to understand how HDFS handles encryption and decryption, not as an improvement to the current system.

\paragraph{hadoop-common-project/hadoop-common/src/main/java/org/apache/hadoop/crypto/CipherSuite.java}
Add a new algorithm in the \textit{CipherSuite} enum:

\begin{lstlisting}[language=Java]
  public enum CipherSuite {
 UNKNOWN("Unknown", 0),
 AES_CTR_NOPADDING("AES/CTR/NoPadding", 16),
 DES_CTR_NOPADDING("DES/CTR/NoPadding", 8);
\end{lstlisting}

The number indicates the size of the needed keys in bytes. DES uses 56 bit keys + 8 bits for internal checks so we need 64 bits in total, 8 bytes.

\paragraph{hadoop-hdfs-project/hadoop-hdfs-client/src/main/proto/hdfs.proto}
Add a new algorithm in the \textit{cipherSuiteProto} enum.

\begin{lstlisting}[language=Java]
 enum CipherSuiteProto {
 UNKNOWN = 1;
 AES_CTR_NOPADDING = 2;
 DES_CTR_NOPADDING = 3;
 }
\end{lstlisting}

\paragraph{hadoop-hdfs-project/hadoop-hdfsclient/src/main/java/org/apache/hadoop/hdfs/protocolPB/PBHelperClient.java}

Add the new cypher in the two convert functions:

\begin{lstlisting}[language=Java]
public static CipherSuite convert(HdfsProtos.CipherSuiteProto proto) {
	switch (proto) {
	 case AES_CTR_NOPADDING:
	 return CipherSuite.AES_CTR_NOPADDING;
	 case DES_CTR_NOPADDING:
	 return CipherSuite.DES_CTR_NOPADDING;
\end{lstlisting}

\begin{lstlisting}[language=Java]
public static HdfsProtos.CipherSuiteProto convert(CipherSuite suite) {
	switch (suite) {
	 case UNKNOWN:
	 return HdfsProtos.CipherSuiteProto.UNKNOWN;
	 case AES_CTR_NOPADDING:
	 return HdfsProtos.CipherSuiteProto.AES_CTR_NOPADDING;
	 case DES_CTR_NOPADDING:
	 return HdfsProtos.CipherSuiteProto.DES_CTR_NOPADDING;
\end{lstlisting}

\paragraph{hadoop-common-project/hadoop-common/src/main/java/org/apache/hadoop/crypto/DesCtrCryptoCodec.java}
This file needs to be created in the given directory. This is the class that is responsible of creating the IV that is needed for the encryption. We don't need to write all the code, we can copy \textit{AesCtrCryptoCodec.java} and change some lines to use DES instead of AES.

\textbf{from:}
\begin{lstlisting}[language=Java]
public abstract class AesCtrCryptoCodec extends CryptoCodec {
 protected static final CipherSuite SUITE = CipherSuite.AES_CTR_NOPADDING;
 private static final int AES_BLOCK_SIZE = SUITE.getAlgorithmBlockSize();
 ...
 Preconditions.checkArgument(initIV.length == AES_BLOCK_SIZE);
 Preconditions.checkArgument(IV.length == AES_BLOCK_SIZE);
 ...
\end{lstlisting}

\textbf{to:}
\begin{lstlisting}[language=Java]
public abstract class DesCtrCryptoCodec extends CryptoCodec {
 protected static final CipherSuite SUITE = CipherSuite.DES_CTR_NOPADDING;
 private static final int DES_BLOCK_SIZE = SUITE.getAlgorithmBlockSize();
 ...
 Preconditions.checkArgument(initIV.length == DES_BLOCK_SIZE);
 Preconditions.checkArgument(IV.length == DES_BLOCK_SIZE);
 ...
\end{lstlisting}

\paragraph{hadoop-common-project/hadoopcommon/src/main/java/org/apache/hadoop/crypto/JceDesCtrCryptoCodec.java}
The same happens with this file, it needs to be created and we can copy the content from \textit{JceAesCtrCryptoCodec.java}. In this file we can find the functions that actually decrypt and encrypt the DEKs.

\textbf{from:}
\begin{lstlisting}[language=Java]
public class JceAesCtrCryptoCodec extends AesCtrCryptoCodec {
 private static final Log LOG =
 LogFactory.getLog(JceAesCtrCryptoCodec.class.getName());
...
public JceAesCtrCryptoCodec() {
...
 public Encryptor createEncryptor() throws GeneralSecurityException {
 return new JceAesCtrCipher(Cipher.ENCRYPT_MODE, provider);
 }
 public Decryptor createDecryptor() throws GeneralSecurityException {
 return new JceAesCtrCipher(Cipher.DECRYPT_MODE, provider);
 }
...
private static class JceAesCtrCipher implements Encryptor, Decryptor {
...
public JceAesCtrCipher(int mode, String provider)
...
cipher.init(mode, new SecretKeySpec(key, "AES"),
...
\end{lstlisting}

\textbf{to:}
\begin{lstlisting}[language=Java]
public class JceDesCtrCryptoCodec extends DesCtrCryptoCodec {
 private static final Log LOG =
 LogFactory.getLog(JceDesCtrCryptoCodec.class.getName());
...
public JceDesCtrCryptoCodec() {
...
 public Encryptor createEncryptor() throws GeneralSecurityException {
 return new JceDesCtrCipher(Cipher.ENCRYPT_MODE, provider);
 }
 public Decryptor createDecryptor() throws GeneralSecurityException {
 return new JceDesCtrCipher(Cipher.DECRYPT_MODE, provider);
 }
...
private static class JceDesCtrCipher implements Encryptor, Decryptor {
...
public JceDesCtrCipher(int mode, String provider)
...
cipher.init(mode, new SecretKeySpec(key, "DES"),
...
\end{lstlisting}

\paragraph{core-site.xml}
After making this changes, HDFS needs to know that \textit{JceDesCtrCryptoCodec} can be used to handle DES keys so this property needs to be added to the core-site.xml.

\begin{lstlisting}[language=xml]
<property>
<name>hadoop.security.crypto.codec.classes.des.ctr.nopadding</name>
<value>org.apache.hadoop.crypto.JceDesCtrCryptoCodec</value>
<description>
Comma-separated list of crypto codec implementations for AES/ECB/NoPadding.
The first implementation will be used if available, others are fallbacks.
</description>
</property>
\end{lstlisting}

\paragraph{hadoop-common-project/hadoopcommon/src/main/java/org/apache/hadoop/crypto/CryptoStreamUtils.java}
As HDFS is prepared to use only AES, some more changes are needed to avoid errors, this is the first one:

\begin{lstlisting}[language=Java]
public static void checkCodec(CryptoCodec codec) {
 if (codec.getCipherSuite() != CipherSuite.AES_CTR_NOPADDING && \
  codec.getCipherSuite() != CipherSuite.DES_CTR_NOPADDING) {
\end{lstlisting}

\paragraph{hadoop-common-project/hadoopcommon/src/main/java/org/apache/hadoop/crypto/key/JavaKeyStoreProvider.java}
The length of a DES key is 56 bits but \textit{javax.crypto.KeyGenerator} creates a key of 64 bits adding some checking numbers. This causes a error when checking if the key is valid, so a small change is needed.

\textbf{from:}
\begin{lstlisting}[language=Java]
 if (options.getBitLength() != 8 * material.length) {
	 throw new IOException("Wrong key length. Required " +
	 options.getBitLength() + ", but got " + (8 * material.length));
 }
\end{lstlisting}

\textbf{to:}
\begin{lstlisting}[language=Java]
 if (options.getCipher().contains("DES")) {
	 if (8 * material.length != 64 && options.getBitLength() != 56) {
	 throw new IOException("Wrong key length. Required " +
	 options.getBitLength() + ", but got " + (8 * material.length));
	 }
 }else {
	 if (options.getBitLength() != 8 * material.length) {
	 throw new IOException("Wrong key length. Required " +
	 options.getBitLength() + ", but got " + (8 * material.length));
	 }
 }
\end{lstlisting}

This is a error that only affects DES, so this change is not needed if another algorithm is used.

\paragraph{/hadoop-common-project/hadoopcommon/src/main/java/org/apache/hadoop/crypto/CryptoCodec.java}
Finally, in this class we will change the default algorithm to DES. 

\begin{lstlisting}[language=Java]
public static CryptoCodec getInstance(Configuration conf) {
 String name = conf.get(HADOOP_SECURITY_CRYPTO_CIPHER_SUITE_KEY,
 HADOOP_SECURITY_CRYPTO_CIPHER_SUITE_DEFAULT);
 String algorithm = "DES/CTR/NoPadding";
 return getInstance(conf, CipherSuite.convert(algorithm));
 }
\end{lstlisting}

\paragraph{hadoop-common-project/hadoopcommon/src/main/java/org/apache/hadoop/crypto/key/KeyProvider.java}
This is the class where the keys are created, no changes are needed in this case because DES is directly supported by \textit{javax.crypto.KeyGenerator}, but if the algorithm that we want to implement is not supported, we would need to change the next function.

\begin{lstlisting}[language=Java]
 protected byte[] generateKey(int size, String algorithm)
 throws NoSuchAlgorithmException {
	 algorithm = getAlgorithm(algorithm);
	 KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
	 keyGenerator.init(size);
	 byte[] key = keyGenerator.generateKey().getEncoded();
	 return key;
 }
\end{lstlisting}

\section{Compilation}
After making these changes, we can compile the code, create a DES key and use it to encrypt an encryption zone.

First, compile \textit{/hadoop-hdfs-project/hadoop-hdfs-client} so the \textit{proto} file is used in the next compilations. To do so, go to the directory and run:

\begin{lstlisting}[language=bash]
	$ mvn compile -DskipTests
\end{lstlisting}

You can skip -DskipTests but it can take a very long time running all the tests.
Then, we need to compile \textit{/hadoop-common-project/hadoop-common}. Once in the directory, the next same command is used.

At last, we need to compile the KMS \textit{/hadoop-common-project/hadoop-kms}.This time the command changes, the KMS is a Tomcat app and needs to be deployed after the compilation. This commands can be used to compile the KMS and deploy it correctly:

\begin{lstlisting}[language=bash]
	$ mvn install -DskipTests
	$ rm -rf src/main/share/hadoop/kms/tomcat/webapps/kms
	$ cp target/kms.war src/main/share/hadoop/kms/tomcat/webapps/
\end{lstlisting}